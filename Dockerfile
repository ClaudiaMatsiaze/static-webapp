FROM ubuntu:18.04
LABEL MAINTAINER="eazytrainingfr@gmail.com"
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y nginx git && \
    apt-get clean
EXPOSE 80
COPY static-website-example/ /var/www/html/
ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]